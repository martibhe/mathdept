# MathDept #

`Beamer` theme for the Department of Mathematics at the University of Oslo.

### Documentation ###

Consult `MathDept-dokumentasjon.pdf`.

### Contact ###

* Martin Helsø¸
* martibhe@math.uio.no
